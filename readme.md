# Students managment system

## Description
In this project we will create a simple student management system. The system will be able to add, edit, delete and list students. Using PostgreSQL as database, Django as backend, React as frontend, Docker and Docker Compose to run the application.

