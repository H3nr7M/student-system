# In this project we use the following commands
# models.py
```
from django.db import models

class Student(models.Model):
    name = models.CharField("Name", max_length=240)
    email = models.EmailField()
    document = models.CharField("Document", max_length=20)
    phone = models.CharField(max_length=20)
    registrationDate = models.DateField("Registration Date", auto_now_add=True)

    def __str__(self):
        return self.name
```
Migrations are Django’s way of propagating changes you make to your models — such as adding a field or deleting a model — into your database schema.

`python manage.py makemigrations`

Next we need to apply the changes to the database itself:

`python manage.py migrate`

Next we going to create a new migration file:

`python manage.py makemigrations --empty --name students students`

This comand will create a new empy migration file in the students/migrations folder. We going to use this file for populate the database with some data.

```
from django.db import migrations

def create_data(apps, schema_editor):
    Student = apps.get_model('students', 'Student')
    Student(name="Joe Silver", email="joe@email.com", document="22342342", phone="00000000").save()
    Student(name="Mary Silver", email="mary@email.com", document="22342343", phone="00000001").save()
    Student(name="John Silver", email="john@email.com", document="22342344", phone="00000002").save()

class Migration(migrations.Migration):

    dependencies = [
        ('students', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(create_data),
    ]
```



