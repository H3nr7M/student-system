#Data for create students in database the first time

from django.db import migrations

def create_data(apps, schema_editor):
    Student = apps.get_model('students', 'Student')
    Student(name="Joe Silver", email="joe@email.com", document="22342342", phone="00000000").save()
    Student(name="Mary Silver", email="mary@email.com", document="22342343", phone="00000001").save()
    Student(name="John Silver", email="john@email.com", document="22342344", phone="00000002").save()

class Migration(migrations.Migration):

    dependencies = [
        ('students', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(create_data),
    ]