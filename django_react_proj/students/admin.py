from django.contrib import admin
from .models import Student
# Register your models here.

class StudentAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'document', 'email', 'phone', 'registrationDate')
    list_display_links = ('id', 'name')
    search_fields = ('name', 'email', 'document')
    list_per_page = 25


admin.site.register(Student, StudentAdmin)

