# Django React Project

This is a simple project to demonstrate how to use React with Django for a REST API for a simple student management system.

## corsheaders

CORS stands for Cross-Origin Resource Sharing, and it's a security feature built into web browsers that restricts web pages from making requests to a different domain than the one the web page came from. This security feature is designed to protect users from malicious websites that may try to steal sensitive information or perform unauthorized actions on their behalf.

However, there are certain scenarios where you might want to make requests from one domain to another. For example, if you are building a web application that uses a separate API backend, you would need to make cross-domain requests. This is where the Django package "django-cors-headers" comes in.

Django-cors-headers is a third-party package that allows you to add CORS headers to your Django responses, which can help bypass the browser's security restrictions and allow your application to make cross-domain requests. The package works by intercepting the response and adding the appropriate CORS headers to it, based on the configuration you provide.

## rest_framework

Django REST framework (often abbreviated as DRF) is a powerful third-party package for Django that makes it easy to build, test, and deploy RESTful APIs. It provides a set of tools and functionality for building APIs that adhere to the REST architectural style, including serialization, authentication, and URL routing.

The main benefits of using Django REST framework are:

- Serialization: DRF provides serializers that allow you to easily convert complex data types (such as Django models) into Python objects that can be easily rendered into JSON or XML.

- Views: DRF provides a set of generic views that allow you to quickly build CRUD (Create, Read, Update, Delete) endpoints for your API.

- Authentication: DRF provides a flexible authentication system that supports token-based authentication, session-based authentication, and OAuth authentication.

- Pagination: DRF provides pagination classes that make it easy to paginate your API responses.

- Documentation: DRF provides built-in documentation generation that makes it easy to create API documentation.

Overall, Django REST framework provides a powerful and flexible framework for building RESTful APIs in Django. It can help you save time and reduce the amount of boilerplate code needed to build APIs, allowing you to focus on the core functionality of your application.

### Pagination

Pagination is the process of dividing a large set of data or content into smaller, more manageable pieces, often referred to as "pages". In web development, pagination is commonly used to display large amounts of data in a way that is easier to navigate and understand for users.

Pagination allows users to navigate through large sets of data in a way that is more efficient and user-friendly. Instead of having to load all of the data at once, which can be slow and overwhelming, users can navigate through smaller subsets of data one page at a time. Each page typically displays a limited number of items, such as 10 or 20, and includes links or buttons to navigate to the previous or next page.

For example, imagine a website that displays a list of blog posts. If there are hundreds or thousands of posts, it would be impractical to display them all on a single page. Instead, the posts could be divided into smaller subsets and displayed on separate pages. The user could then navigate through the pages to find the posts they are interested in.

In web development, pagination can be implemented using various technologies and frameworks, such as Django's built-in pagination classes, JavaScript libraries like jQuery or React, or CSS frameworks like Bootstrap.

#### re_path

re_path is a function provided by Django for defining URL patterns based on regular expressions. It is similar to the path function, but instead of using simple string patterns, it allows you to define more complex patterns using regular expressions.

## Model


### Data migration

We use the `python manage.py makemigrations --empty --name students students` see the migrations -> 0002_students.py file. In this file creates initial data, just so that our database isn’t empty when the API starts.

## Serializers

The serializers module provides a set of classes and functions that allow you to convert complex data types (such as Django models) into a format that can be easily rendered into JSON, XML, or other content types that are commonly used for web APIs.

The main purpose of the serializers module is to serialize and deserialize data that is sent and received by your RESTful API. When a client makes a request to your API, the data in that request may be in a format that is not directly usable by your application, such as JSON or XML. The serializers module provides a set of classes and functions that allow you to convert that data into a Python object that can be easily processed and manipulated by your application.