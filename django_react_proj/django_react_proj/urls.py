from django.contrib import admin
from django.urls import path, re_path
# re_path is used to match regular expressions
from students import views

urlpatterns = [
    path('admin/', admin.site.urls), # admin page
    re_path(r'^api/students/$', views.students_list),
    re_path(r'^api/students/([0-9])$', views.students_detail),
]
# In this example the URL matches 