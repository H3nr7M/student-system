import React, { Component, Fragment } from "react";
import axios from "axios";
import { API_URL } from "../constants";
import "../static/styles/ConfirmRemovalModal.css";

class ConfirmRemovalModal extends Component {
  state = {
    showAlert: false
  };

  toggleAlert = () => {
    this.setState(previous => ({
      showAlert: !previous.showAlert
    }));
  };

  deleteStudent = pk => {
    axios.delete(API_URL + pk).then(() => {
      this.props.resetState();
      this.toggleAlert();
    });
  };

  render() {
    return (
      <Fragment>
        <button className="btn btn-danger" onClick={() => this.toggleAlert()}>
          Remove
        </button>
        {this.state.showAlert && (
          <div className="confirm-removal-modal">
            <div className="modal-content">
              <p>Do you really want to delete the student?</p>
              <hr />
              <div className="buttons">
                <button
                  type="button"
                  className="btn btn-primary"
                  onClick={() => this.deleteStudent(this.props.pk)}
                >
                  Yes
                </button>
                <button
                  type="button"
                  className="btn btn-secondary"
                  onClick={() => this.toggleAlert()}
                >
                  Cancel
                </button>
              </div>
            </div>
          </div>
        )}
      </Fragment>
    );
  }
}

export default ConfirmRemovalModal;
