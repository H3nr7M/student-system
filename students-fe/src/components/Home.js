import React, { Component } from "react"; //use class components
import StudentList from "./StudentList";
import NewStudentModal from "./NewStudentModal";
import '../static/styles/Home.css'

import axios from "axios"; //for managing HTTP requests more easily

import { API_URL } from "../constants"; //access to the endpoint URL of the API

class Home extends Component {
  state = { //setup a initial state
    students: []
  };
//The componentDidMount method is called when the component mounts. It calls the resetState method, which retrieves the list of students from the API and updates the component's state.
  componentDidMount() { 
    this.resetState();
  }
  // set the state of students to the data retrieved from the API
  getStudents = () => {
    axios.get(API_URL).then(res => this.setState({ students: res.data }));
  };

  resetState = () => {
    this.getStudents();
  };

  render() {
    return (
      <div className="container">
      {/* row for diaplaying student list */}
        <div className="row">
          <div className="col">
            <StudentList //this component receives a props students and resetState
              students={this.state.students}
              resetState={this.resetState}
            />
          </div>
        </div>
        {/* row for add new student */}
        <div className="row"> 
          <div className="col">
            <NewStudentModal create={true} resetState={this.resetState} />
          </div>
        </div>
      </div>
    );
  }
}

export default Home;