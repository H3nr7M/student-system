import React, { Component } from "react";
import "../static/styles/Header.css";

// class component
class Header extends Component {
  render() {
    return (
      <div className="text-center">
      <h1>student management system</h1>
        <img
          src={require(`../static/images/uag.png`)}
          width="300"
          className="img-thumbnail"
          alt="LogRocket"
        />
        <hr />
      </div>
    );
  }
}

export default Header;