import React, { Component, Fragment } from "react";
import NewStudentForm from "./NewStudentForm";
import '../static/styles/NewStudentModal.css'

class NewStudentModal extends Component {
  state = {
    showAlert: false
  };

  toggleAlert = () => {
    this.setState(previous => ({
      showAlert: !previous.showAlert
    }));
  };

  render() {
    const create = this.props.create;

    var title = "Editing Student";
    var button = (
      <button className="btn-edit" onClick={this.toggleAlert}>Edit</button>
    );
    if (create) {
      title = "Creating New Student";

      button = (
        <button
          className="btn-create float-right"
          onClick={this.toggleAlert}
        >
          Create New
        </button>
      );
    }

    return (
      <Fragment>
        {button}
        <div className={this.state.showAlert ? "modal display-block" : "modal display-none"}>
          <div className="modal-content">
            <span className="close" onClick={this.toggleAlert}>&times;</span>
            <div className="modal-header">{title}</div>
            <div className="modal-body">
              <NewStudentForm
                resetState={this.props.resetState}
                toggle={this.toggleAlert}
                student={this.props.student}
              />
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default NewStudentModal;
