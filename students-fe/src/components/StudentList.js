import React, { Component } from "react";
import NewStudentModal from "./NewStudentModal";
import ConfirmRemovalModal from "./ConfirmRemovalModal";
import '../static/styles/StudentList.css';

class StudentList extends Component {
  render() {
    const students = this.props.students;
    return (
      <div className="student-list">
        <div className="header-row">
          <div className="cell">Name</div>
          <div className="cell">Email</div>
          <div className="cell">Document</div>
          <div className="cell">Phone</div>
          <div className="cell">Registration</div>
          <div className="cell"></div>
        </div>
        {!students || students.length <= 0 ? (
          <div className="empty-row">
            <b>Ops, no one here yet</b>
          </div>
        ) : (
          students.map((student) => (
            <div className="data-row" key={student.pk}>
              <div className="cell">{student.name}</div>
              <div className="cell">{student.email}</div>
              <div className="cell">{student.document}</div>
              <div className="cell">{student.phone}</div>
              <div className="cell">{student.registrationDate}</div>
              <div className="cell">
                <NewStudentModal
                  create={false}
                  student={student}
                  resetState={this.props.resetState}
                />
                &nbsp;&nbsp;
                <ConfirmRemovalModal
                  pk={student.pk}
                  resetState={this.props.resetState}
                />
              </div>
            </div>
          ))
        )}
      </div>
    );
  }
}

export default StudentList;
