// The Component it's for a class based component and fragment is for the root element
import React, { Component, Fragment } from "react";
import Header from "./components/Header";
import Home from "./components/Home";
import './static/styles/App.css';

class App extends Component {
  render() {
    return (
      <Fragment>
        <div className="body">
        <Header />
        <Home />
        </div>
      </Fragment>
    );
  }
}

export default App;